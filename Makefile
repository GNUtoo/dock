DESTDIR ?= /usr/local

.PHONY: all clean

all: dock

dock: dock.c
	$(CC) $< -o $@
clean:
	rm -f dock
install: dock
	install -d    $(DESTDIR)/bin
	install $< -t $(DESTDIR)/bin
