/* Based on src/southbridge/intel/common/gpio.c in Coreboot
 * which has: SPDX-License-Identifier: GPL-2.0-only
 */
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/io.h>

bool run_as_root(void)
{
	int rc;

	rc = iopl(3);
	if (rc == -1) {
		rc = errno;
		printf("iopl failed with error: %s (%d)\n",
				strerror(rc), rc);
		return false;
	}

	return true;
}

bool sysfs_cmp(char *path, char *content)
{
	char *buffer;
	int count;
	int fd;
	int rc;
	ssize_t offset = 0;
	ssize_t remaining = 0;

	fd = open(path, O_RDONLY);
	if (fd == -1) {
		rc = errno;
		printf("%s: open %s error %d: %s\n",
		       __func__,
		       path,
		       rc,
		       strerror(rc));
		return false;
	}

	remaining = strlen(content);
	buffer = malloc(remaining + 1);

	do {
		ssize_t count;

		count = read(fd, buffer + offset, strlen(content) - offset);
		if (count == -1) {
			rc = errno;
			printf("%s: read %s error %d: %s\n",
			       __func__,
			       path,
			       rc,
			       strerror(rc));
			free(buffer);
			return false;
		}
		remaining -= count;
		offset += count;
	} while (remaining > 0);

	buffer[offset] = '\0';


	if (strcmp(content, buffer)) {
		free(buffer);
		return false;
	}

	free(buffer);
	return true;

}

bool is_compatible(void)
{
	bool equals;

	equals = sysfs_cmp("/sys/class/dmi/id/board_version",
			   "ThinkPad X200");
	if (!equals) {
		printf("This computer is not compatible with the dock utility\n");
		return false;
	}

	return true;
}

int set_gpio(int gpio, int value)
{
	int rc;
	uint32_t config;

	config = inl(1420);
	config &= ~(1<< gpio);
	if (!!value)
		config |= (1 << gpio);

	outl(config, 1420);
}

int main(int argc, char **argv)
{
	if (!run_as_root())
		exit(1);

	if (!is_compatible())
		exit(1);

	system("modprobe ec_sys write_support=1");
	system("ec -w 0x02 -v 0x1");
	set_gpio(28, 1);
}
